+++ 
draft = false
date = 2022-07-11T08:14:56-05:00
title = ""
slug = "" 
+++
# About Me

Hello!

My name is Paul Lemmermann, and I am an amateur programmer. I have successfully submitted some patches to the Linux kernel.

My favorite programming languages are C and [Rust](https://rust-lang.org).

If you would like to contact me, my email is [thepaulodoom@thepaulodoom.com](mailto:thepaulodoom@thepaulodoom.com).

Also, my PGP key is located [here](/pubkey.gpg).

---
title: "Record Collection"
date: 2024-06-29T12:49:10-05:00
draft: false
---

# Albums

| Album Title | Artist | Year | Album Cover |
|-------------|--------|------|-------------|
| Layla and Other Assorted Love Songs | Derek and the Dominos | 1970 | ![Layla and Other Assorted Love Songs](https://upload.wikimedia.org/wikipedia/en/a/a7/LaylaCover.jpg) |
| Nothing but the Best | Frank Sinatra | 2008 | ![Nothing but the Best](https://upload.wikimedia.org/wikipedia/en/2/2b/Frank_sinatra_front-Nothing_But_the_Best_%28album%29.jpg) |
| Blue Train | John Coltrane | 1958 | ![Blue Train](https://upload.wikimedia.org/wikipedia/en/6/68/John_Coltrane_-_Blue_Train.jpg) |
| The Doors | The Doors | 1967 | ![The Doors](https://upload.wikimedia.org/wikipedia/en/9/98/TheDoorsTheDoorsalbumcover.jpg) |
| The Stranger | Billy Joel | 1977 | ![The Stranger](https://upload.wikimedia.org/wikipedia/en/f/f5/Thestranger1977.jpg) |
| Whipped Cream & Other Delights | Herb Alpert & the Tijuana Brass | 1965 | ![Whipped Cream & Other Delights](https://upload.wikimedia.org/wikipedia/en/d/da/HA_WhippedCream.jpg) |
| Carnival | Maynard Ferguson | 1978 | ![Carnival](https://upload.wikimedia.org/wikipedia/en/5/53/MF_Carnival.jpg) |
| If I Can Dream | Elvis Presley, Royal Philharmonic Orchestra | 2015 | ![If I Can Dream](https://upload.wikimedia.org/wikipedia/en/6/65/Elvis_If_I_Can_Dream.jpeg) |
| Elvis Is Back! | Elvis Presley | 1960 | ![Elvis Is Back!](https://upload.wikimedia.org/wikipedia/en/6/64/Elvis_is_Back%21.jpg) |
| Slowhand | Eric Clapton | 1977 | ![Slowhand](https://upload.wikimedia.org/wikipedia/en/e/e7/EricClapton-Slowhand.jpg) |
| 40 Greatest Hits | Hank Williams | 1978 | ![40 Greatest Hits](https://upload.wikimedia.org/wikipedia/en/0/08/40_Greatest_Hits_%28Hank_Williams%2C_Sr._album%29.jpeg) |
| The River | Bruce Springsteen | 1980 | ![The River](https://upload.wikimedia.org/wikipedia/en/c/cc/The_River_%28Bruce_Springsteen%29_%28Front_Cover%29.jpg) |
| ...And Justice For All | Metallica | 1988 | ![...And Justice For All](https://upload.wikimedia.org/wikipedia/en/b/bd/Metallica_-_...And_Justice_for_All_cover.jpg) |
| Tommy | The Who | 1969 | ![Tommy](https://upload.wikimedia.org/wikipedia/en/1/19/Tommyalbumcover.jpg) |
| How Great Thou Art | Elvis Presley | 1967 | ![How Great Thou Art](https://upload.wikimedia.org/wikipedia/en/5/50/Elvis_Presley_How_Great_Thou_Art_LP_Cover.jpg) |
| Bridge Over Troubled Water | Simon and Garfunkel | 1970 | ![Bridge Over Troubled Water](https://upload.wikimedia.org/wikipedia/en/4/41/Simon_and_Garfunkel%2C_Bridge_over_Troubled_Water_%281970%29.png) |
| Elvis' Golden Records Volume 3 | Elvis Presley | 1963 | ![Elvis' Golden Records Volume 3](https://upload.wikimedia.org/wikipedia/en/6/61/Elvis_Presley_original_LP_cover_for_%22Elvis%27_Golden_Records_Vol._3%22.jpg) |
| Gentle on My Mind | Glen Campbell | 1967 | ![Gentle on My Mind](https://upload.wikimedia.org/wikipedia/en/6/6e/Glen_Campbell_Gentle_on_My_Mind_album_cover.jpg) |
| As Recorded at Madison Square Garden | Elvis Presley | 1972 | ![As Recorded at Madison Square Garden](https://upload.wikimedia.org/wikipedia/en/f/f3/Elvis_Presley_Madison_Square_Garden_LP_Cover.jpg) |
| Aloha from Hawaii via Satellite | Elvis Presley | 1973 | ![Aloha from Hawaii via Satellite](https://upload.wikimedia.org/wikipedia/en/5/55/Aloha_from_Hawaii_Via_Satellite.jpg) |
| Bookends | Simon & Garfunkel | 1968 | ![Bookends](https://upload.wikimedia.org/wikipedia/en/c/ca/Simon_and_Garfunkel%2C_Bookends_%281968%29.png) |
| Who's Next | The Who | 1971 | ![Who's Next](https://upload.wikimedia.org/wikipedia/en/4/44/Whosnext.jpg) |
| ...And I Know You Wanna Dance | Johnny Rivers | 1966 | ![...And I Know You Wanna Dance](https://e.snmc.io/i/600/w/a0211833a6414d25f49cda9d50da8c90/6610744/johnny-rivers-and-i-know-you-wanna-dance-Cover-Art.jpg) |
| Great Gershwin | Paul Whiteman And His Orchestra, George Gershwin | 1956 | ![Great Gershwin](https://i.discogs.com/-MN9VRvqwONijDMywlMCgxAOsja1WwQ6lAZ-NhMSXxA/rs:fit/g:sm/q:90/h:600/w:589/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTE2MDcy/NDIyLTE2MDI5NDQ2/ODEtMjQ4Mi5qcGVn.jpeg) |
| Born To Run | Bruce Springsteen | 1975 | ![Born To Run](https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Born_to_Run_%28Front_Cover%29.jpg/220px-Born_to_Run_%28Front_Cover%29.jpg) |
| Meddle | Pink Floyd | 1971 | ![Meddle](https://upload.wikimedia.org/wikipedia/en/thumb/d/d4/MeddleCover.jpeg/220px-MeddleCover.jpeg) |
| The Dark Side of the Moon | Pink Floyd | 1973 | ![The Dark Side of the Moon](https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/Dark_Side_of_the_Moon.png/220px-Dark_Side_of_the_Moon.png) |
| Wish You Were Here | Pink Floyd | 1975 | ![Wish You Were Here](https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Pink_Floyd%2C_Wish_You_Were_Here_%281975%29.png/220px-Pink_Floyd%2C_Wish_You_Were_Here_%281975%29.png) |
| The Wall | Pink Floyd | 1979 | ![The Wall](https://upload.wikimedia.org/wikipedia/en/thumb/1/13/PinkFloydWallCoverOriginalNoText.jpg/220px-PinkFloydWallCoverOriginalNoText.jpg) |
| Eddie Money | Eddie Money | 1977 | ![Eddie Money](https://upload.wikimedia.org/wikipedia/en/thumb/2/26/Eddiemoneyeddiemoney.jpg/220px-Eddiemoneyeddiemoney.jpg) |
| Life for the Taking | Eddie Money | 1978 | ![Life for the Taking](https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/Eddiemoneylifeforthetaking.jpg/220px-Eddiemoneylifeforthetaking.jpg) |
| Dirty Deeds Done Dirt Cheap | AC/DC | 1976 | ![Dirty Deeds Done Dirt Cheap](https://upload.wikimedia.org/wikipedia/en/thumb/5/51/Dirty_Deeds_Done_Dirt_Cheap_%28ACDC_album_-_cover_art0.jpg/220px-Dirty_Deeds_Done_Dirt_Cheap_%28ACDC_album_-_cover_art0.jpg) |
| Let There Be Rock | AC/DC | 1977 | ![Let There Be Rock](https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/ACDC-LetThereBeRock.jpg/220px-ACDC-LetThereBeRock.jpg) |
| Back in Black | AC/DC | 1980 | ![Back in Black](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/ACDC_Back_in_Black_cover.svg/220px-ACDC_Back_in_Black_cover.svg.png) |
| Desperado | The Eagles | 1973 | ![Desperado](https://upload.wikimedia.org/wikipedia/en/thumb/c/c0/The_Eagles_-_Desperado.jpg/220px-The_Eagles_-_Desperado.jpg) |
| Red Rose Speedway | Paul McCartney and Wings | 1973 | ![Red Rose Speedway](https://upload.wikimedia.org/wikipedia/en/thumb/7/72/Wings%2C_Red_Rose_Speedway_%281973%29.png/220px-Wings%2C_Red_Rose_Speedway_%281973%29.png) |
| Wings At The Speed Of Sound | Paul McCartney and Wings | 1976 | ![Wings At The Speed of Sound](https://upload.wikimedia.org/wikipedia/en/thumb/a/a5/Wings_at_the_Speed_of_Sound_album_cover.png/220px-Wings_at_the_Speed_of_Sound_album_cover.png) |
| Foreigner | Foreigner | 1977 | ![Foreigner](https://upload.wikimedia.org/wikipedia/en/thumb/7/70/Foreigner_debut.jpg/220px-Foreigner_debut.jpg) |
| Made In Japan | Deep Purple | 1973 | ![Made In Japan](https://upload.wikimedia.org/wikipedia/en/thumb/c/c5/Deep_Purple_Made_in_Japan.jpg/220px-Deep_Purple_Made_in_Japan.jpg) |
| The Six Wives of Henry VIII | Rick Wakeman | 1973 | ![The Six Wives of Henry VIII](https://upload.wikimedia.org/wikipedia/en/d/da/SixWives_Wakeman_Album.jpg) |
| The Myths and Legends of King Arthur and the Knights of the Round Table | Rick Wakeman | 1975 | ![The Myths and Legends of King Arthur and the Knights of the Round Table](https://upload.wikimedia.org/wikipedia/en/thumb/8/85/King_Wakeman_Album.jpg/220px-King_Wakeman_Album.jpg) |
| No Earthly Connection | Rick Wakeman | 1976 | ![No Earthly Connection](https://i.discogs.com/2YGO1r5TFaYsg4CpkFwxTx8DHhqnh_ZXM5Pr9dj6E1Q/rs:fit/g:sm/q:90/h:600/w:599/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTc3Njgz/MS0xMjg0NjcyNDU0/LmpwZWc.jpeg) |
| Rick Wakeman's Criminal Record | Rick Wakeman | 1977 | ![Rick Wakeman's Criminal Record](https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/Criminal_record_lp.jpg/220px-Criminal_record_lp.jpg) |
| A New World Record | Electric Light Orchestra | 1976 | ![A New World Record](https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/ELO_A_New_World_Record.jpg/220px-ELO_A_New_World_Record.jpg) |
| Out of the Blue | Electric Light Orchestra | 1977 | ![Out of the Blue](https://upload.wikimedia.org/wikipedia/en/thumb/5/5a/ELO-Out_of_the_Blue_Lp.jpg/220px-ELO-Out_of_the_Blue_Lp.jpg) |
| Some Girls | The Rolling Stones | 1978 | ![Some Girls](https://upload.wikimedia.org/wikipedia/en/thumb/6/6c/Some_Girls.png/220px-Some_Girls.png) |
| Viva Terlingua! | Jerry Jeff Walker | 1973 | ![Viva Terlingua!](https://upload.wikimedia.org/wikipedia/en/thumb/3/3d/JerryJeffWalkerVivaTerlingua.jpg/220px-JerryJeffWalkerVivaTerlingua.jpg) |
| "Weird Al" Yankovic's Greatest Hits | "Weird Al" Yankovic | 1988 | !["Weird Al" Yankovic's Greatest Hits](https://i.discogs.com/Fnzwf1vQPSZnRZHC4pRRPwvU8C3CVbO5yEVNtGnTSPw/rs:fit/g:sm/q:90/h:588/w:600/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTEzMzQz/ODMtMTYyODk4MDIx/Ni02ODk0LmpwZWc.jpeg) |
| ...Today | It's a Beautiful Day | 1973 | ![...Today](https://i.discogs.com/yuaVJYtkBO_IkzX3pH5kPhl7jiu_-gRD543YvukmCxU/rs:fit/g:sm/q:90/h:586/w:595/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTI5MjM0/NDYtMTMwOTE0NDQ0/OC5qcGVn.jpeg) |
| Yessongs | Yes | 1973 | ![Yessongs](https://upload.wikimedia.org/wikipedia/en/thumb/6/65/Yessongs_front_cover.png/220px-Yessongs_front_cover.png) |
| Relayer | Yes | 1974 | ![Relayer](https://upload.wikimedia.org/wikipedia/en/thumb/9/92/Relayer_front_cover.jpg/220px-Relayer_front_cover.jpg) |
| Greatest Hits | The Doors | 1980 | ![Greatest Hits](https://upload.wikimedia.org/wikipedia/en/b/be/The_Doors_-_Greatest_Hits_-1980-.jpg) |

# Singles

| A Side | B Side | Artist | Year | Image |
|--------|--------|--------|------|-------|
| Take Five | Blue Rondo à la Turk | Dave Brubeck Quartet | 1959 | ![Take Five](/records/takefive.jpg) |
| Good Hearted Woman | Suspicious Minds | Waylon Jennings | 1977 | ![Good Hearted Woman](/records/goodheartedwoman.jpg) |

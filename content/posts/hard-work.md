+++ 
draft = false
date = 2023-07-19T22:39:51-05:00
title = "On Hard Work and Why It Is Important"
description = ""
slug = ""
authors = ["Paul Lemmermann"]
tags = ["Philosophy", "Christianity"]
categories = []
externalLink = ""
series = []
+++
According to George S. Patton, "Accept the challenges so that you can feel the 
exhilaration of victory."

Hard work is tremendously important in society. Obviously, without hard work, we
wouldn't have the luxuries we have today, nor would we have the necessities, like
food, shelter, and water. Whatever interactions we do, there is always someone
who works, or has worked, hard in the process: whenever we buy food from the
grocery store, there is initially the farm workers who have prepared the food,
maybe even manufacturers who have created and designed the machinery used in
said preparation process, and then there are those who have stocked the shelves
and those involved in the checkout process. There is work at every point in the
hierarchy.

Obviously, in each of the jobs listed there is a discrepancy in the amount of
work and responsibilities entailed, but every job is equally important in the
process. It is important to be grateful to each of the aforementioned positions
because without them, you wouldn't have your food. This is also generalized to
any field, such as the power grid, water grid, et. al. So be grateful to these
workers.

That is one of the many reasons to work hard in your position. You are a cog,
likely an important one, in society. Society needs every cog to be functioning
so that it can run smoothly. Sure, if you give up, there are often other people
that can fill in your job for you, but then you are filling a redundant seat and
simply wasting money for your employer, increasing the workload of your
coworkers, who are doing more work for the same amount of pay because of your
unwillingness to work hard. Your unwillingness to do hard work can be then
boiled down into laziness and, ultimately, selfishness. 

Hard work is not only a plus for society, but for you as a whole. Once you get
finished with a hard job, like a _really_ hard job, and do it right, you will
feel the "exhilaration" and satisfaction of finishing a job for the day, and you
will be rewarded for that, whether it be only for the feelings of satisfaction,
or by compensation from your employer.

So go out, work hard, not only for the benefit of you, but for the benefit of
society! And be grateful for those in society that work hard, so that we may
reap the benefits from them!

"For we are his workmanship, created in Christ Jesus for good works, which God
prepared beforehand, that we should walk in them." Ephesians 2:10 (ESV)

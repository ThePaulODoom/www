+++ 
draft = false
date = 2022-07-10T10:12:14-05:00
title = "Colin and Nick's Anchored in Cruising Podcast"
description = ""
slug = ""
authors = ["Paul Lemmermann"]
tags = ["Cruising"]
categories = []
externalLink = ""
series = []
+++

If you are a big fan of cruising, and want the latest cruise news in a short, digestible, and fun format, check out the [Anchored in Cruising Podcast](https://youtube.com/playlist?list=PLK7A-Tu2dmAIwmm86DQFXpMRMaZWdh-JA) done by [Cruise With Colin And Nick](https://www.youtube.com/channel/UCyDKhc5LQKSY6S-1crDjsyA).

You can see more of Nick Jay's work [here](https://www.youtube.com/channel/UC4mjorEygjuvje6b448alkA) and see more of Colin Stewart's work [here](https://www.instagram.com/colinstewart__/).
